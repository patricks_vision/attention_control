import os

from yacs.config import CfgNode

from src.configs.augmentations_config import (
    get_torchvision_transforms,
    test_augmentations,
    train_augmentations,
    val_augmentations,
)
from src.data.DataAugmentations import DataAugmentations
from src.data.DriverDataModule import DriverDataModule


def load_env_file(file_path):
    with open(file_path, 'r') as file:
        for line in file:
            key, value = line.strip().split('=', 1)
            os.environ[key] = value


def get_datamodule(config: CfgNode = None):
    base_transforms = get_torchvision_transforms(
        image_size=config.DATAMODULE.IMG_SIZE
    )

    augmentations = DataAugmentations(
        base_transforms=base_transforms,
        train_augmentations=train_augmentations,
        val_augmentations=val_augmentations,
        test_augmentations=test_augmentations
    )
    # создаем DataModule-экземпляр
    data_module = DriverDataModule(
        image_size=config.DATAMODULE.IMG_SIZE,
        batch_size=config.DATAMODULE.BATCH_SIZE,
        train_dir=config.DATASET.TRAIN_DIR,
        val_dir=config.DATASET.VAL_DIR,
        test_dir=config.DATASET.TEST_DIR,
        num_workers=config.DATAMODULE.NUM_WORKERS,
        augmentations=augmentations
    )

    return data_module
