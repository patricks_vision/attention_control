import argparse
import os
from typing import List, Optional

import mlflow
import pytorch_lightning as pl
import torch
from dotenv import find_dotenv
from lightning.pytorch.loggers import MLFlowLogger
from pytorch_lightning import Trainer
from pytorch_lightning.callbacks import LearningRateMonitor
from pytorch_lightning.callbacks.early_stopping import EarlyStopping
from pytorch_lightning.callbacks.model_checkpoint import ModelCheckpoint
from yacs.config import CfgNode

from src.configs.base_config import combine_config
from src.models.BaseClassifier import BaseClassifier
from src.models.PerformanceChecker import PerformanceChecker
from src.utils.utils import get_datamodule, load_env_file


def init_train_components(config: CfgNode):
    # инициализируем модель и модуль для тренировки модели
    data_module = get_datamodule(config=config)
    model = BaseClassifier(eta=config.LEARNING.ETA,
                             model=config.LEARNING.BASE_MODEL,
                             device=config.LEARNING.DEVICE,
                             class_names=config.DATASET.CLASSES)
    callbacks = get_callbacks(config)
    mlf_logger = MLFlowLogger(experiment_name=config.MLFLOW.EXPERIMENT,
                            log_model=config.MLFLOW.LOG_CHECKPOINTS,
                            run_id=mlflow.active_run().info.run_id)

    trainer = Trainer(
        max_epochs=config.LEARNING.MAX_EPOCHS,
        accelerator=config.LEARNING.DEVICE,
        callbacks=callbacks,
        logger=mlf_logger
    )

    train_components = {
        'data_module': data_module,
        'model': model,
        'trainer': trainer
    }

    return train_components


def train(config: CfgNode,
          mlflow_run_id: Optional[str] = None):
    """Функция для обучения модели.

    Args:
        config (CfgNode): Конфигурационный файл.
    """
    pl.seed_everything(config.LEARNING.SEED)

    experiment = mlflow.set_experiment(config.MLFLOW.EXPERIMENT)
    with mlflow.start_run(
        run_name=config.MLFLOW.RUN_NAME,
        experiment_id=experiment.experiment_id,
    ):
        mlflow.log_artifact(config.CFG_INFO.CFG_PATH)
        mlflow.log_artifact(config.CFG_INFO.AUG_PATH)
        train_components = init_train_components(config)
        train_components['trainer'].fit(train_components['model'],
                                        train_components['data_module'],
                                        ckpt_path=config.LEARNING.CHECKPOINT_PATH)
        train_components['trainer'].test(train_components['model'],
                                         datamodule=train_components['data_module'])

        performance_checker = PerformanceChecker(
            model=train_components['model'],
            cfg=cfg)
        performance_checker.start_check()

        os.makedirs(os.path.dirname(config.LOGGING.SAVING_MODEL_PATH), exist_ok=True)
        torch.save(train_components['model'], config.LOGGING.SAVING_MODEL_PATH)

        if config.MLFLOW.LOG_MODEL:
            mlflow.pytorch.log_model(train_components['model'], "model")


def get_callbacks(config: CfgNode) -> List[object]:
    """Возвращает список коллбэков для обучения модели.
    На данный момент используются ранние остановки, отслеживание learning rate,
    чекпоинты модели.

    Args:
        config (CfgNode): Конфигурационный объект.

    Returns:
        List[object]: Список коллбэков.
    """

    # список для отслеживания lr, ранних остановок, сохранения весов
    callbacks = [
        ModelCheckpoint(
            dirpath=config.CHECKPOINT.CKPT_PATH,
            filename=config.CHECKPOINT.FILENAME,
            save_top_k=config.CHECKPOINT.SAVE_TOP_K,
            monitor=config.CHECKPOINT.CKPT_MONITOR,
            mode=config.CHECKPOINT.CKPT_MODE
        ),
        LearningRateMonitor(
            logging_interval=config.LOGGING.LOGGING_INTERVAL
        ),
        EarlyStopping(
            monitor=config.ES.MONITOR,
            min_delta=config.ES.MIN_DELTA,
            patience=config.ES.PATIENCE,
            verbose=config.ES.VERBOSE,
            mode=config.ES.MODE
        )
    ]

    return callbacks


def get_args_parser() -> argparse.Namespace:
    """Функция для парсинга аргументов, указанных при вызове скрипта

    Returns:
        argparse.Namespace: объект, который содержит аргументы в виде атрибутов
    """
    parser = argparse.ArgumentParser(description="Training")
    parser.add_argument("-config_path",
                        default='src/configs/params.yaml',
                        type=str,
                        help="Путь к конфигурационному файлу")
    return parser.parse_args()


if __name__ == "__main__":
    load_env_file(find_dotenv())
    args = get_args_parser()
    cfg_path = args.config_path
    cfg = combine_config(cfg_path)
    train(cfg)
