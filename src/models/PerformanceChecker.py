import os
import time

import mlflow
import torch
from PIL import Image

from src.configs.augmentations_config import (
    get_torchvision_transforms,
    train_augmentations,
)
from src.data.DataAugmentations import DataAugmentations


class PerformanceChecker():
    def __init__(self, model, cfg) -> None:
        self.model = model
        self.cfg = cfg

    def get_example_paths(self):
        path_to_dataset = self.cfg.DATASET.PERFORMANCE_DATA_PATH
        file_list = os.listdir(path_to_dataset)
        file_paths = [
            os.path.join(path_to_dataset, filename) for filename in file_list
        ]

        return file_paths

    def get_batch(self):
        # получаем пути к картинкам
        file_paths = self.get_example_paths()

        # получаем трансформации
        base_transforms = get_torchvision_transforms(
            image_size=self.cfg.DATAMODULE.IMG_SIZE
        )
        augmentations = DataAugmentations(
            base_transforms=base_transforms,
            train_augmentations=train_augmentations
        )

        # батч из картинок
        batch = []
        for fp in file_paths:
            transform_img = augmentations.test_transforms(Image.open(fp))
            batch.append(transform_img)
        return batch

    def start_check(self) -> None:
        self.batch = self.get_batch()
        perfomance_times = dict()

        ms_image, ms_batch = self.predict(device='cpu')
        perfomance_times['cpu_time_image'] = ms_image
        perfomance_times['cpu_time_batch'] = ms_batch

        if torch.cuda.is_available():
            ms_image, ms_batch = self.predict(device='cuda')
            perfomance_times['gpu_time_image'] = ms_image
            perfomance_times['gpu_time_batch'] = ms_batch

        mlflow.log_metrics(perfomance_times)

    def predict(self, device: str) -> float:
        """
        Performs emotion classification on the input texts.

        Args:
            texts (List[str]): A list of input texts to classify.

        Returns:
            Tuple[List[str], List[float]]: A tuple containing a list of predicted
            emotion classes corresponding to the input texts and
            a list of corresponding probabilities.
        """

        self.model.to(device)
        self.model.eval()
        with torch.inference_mode():
            # измеряем среднюю скорость для одного изображения
            t = time.time()
            for image in self.batch:
                _ = self.model(image.unsqueeze(0).to(device))
            ms_image = (time.time() - t) / len(self.batch) * 1000

            # измеряем скорость для батча
            t = time.time()
            batch = torch.stack(self.batch)
            _ = self.model(batch.to(device))
            ms_batch = (time.time() - t) * 1000
        return ms_image, ms_batch
