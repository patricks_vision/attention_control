import os
from typing import List, Optional

import numpy as np
import pytorch_lightning as pl
import timm
import torch
from sklearn.metrics import accuracy_score, classification_report, f1_score
from torch import nn


class BaseClassifier(pl.LightningModule):
    def __init__(self,
                 model: str = "efficientnet_b0",
                 in_channels: int = 3,
                 num_classes: int = 10,
                 class_names: Optional[List[str]] = None,
                 eta: float = 3e-4,
                 device: str = 'cpu',
                 cls_report_path: Optional[str] = None,
                 **kwargs) -> None:
        """Бейзлайн модель EfficientNet, предобученная
         и с базовой архитектурой. Класс отнаследован
         от pl.LightningModule

        Args:
            model (str, optional): Версия модели, которая будет использоваться.
            По умолчанию "efficientnet-b0".
            in_channels (int, optional): Количество каналов на входе модели.
            По умолчанию 3.
            num_classes (int, optional): Количество классов.
            По умолчанию 10.
            eta (int, optional): Параметр learning rate для оптимизатора.
            По умолчанию 3e-4.
        """
        super().__init__()
        self.model = timm.create_model(
            model,
            pretrained=True,
            num_classes=num_classes
        )

        self.num_classes = num_classes
        self.class_names = class_names
        self.criterion = nn.CrossEntropyLoss()
        self.hparams.eta = eta
        self.model_device = device

        self.preds_stage = {
                    "train": {"loss": [],
                              "preds": [],
                              "targets": []},
                    "valid": {"loss": [],
                              "preds": [],
                              "targets": []},
                    "test": {"loss": [],
                              "preds": [],
                              "targets": []}
                    }
        self.cls_report_path = cls_report_path

    def forward(self, x: torch.Tensor = None) -> torch.Tensor:
        """Функция для forward pass модели

        Args:
            x (torch.Tensor, required): Тензор с изображениями
            для forward pass. По умолчанию None.

        Returns:
            torch.Tensor: Возвращает тензор с результатом прохода модели.
        """
        return self.model(x)

    def shared_step(self,
                    sample=None,
                    stage: str = None) -> torch.Tensor:
        """Общий шаг для всех этапов - обучения,
        валидации и тестирования модели.

        Args:
            sample (required): Тензор с изображениями. По умолчанию None.
            stage (str, required): Этап, может принимать значения из списка:
            ['train', 'valid', 'test']. По умолчанию None.

        Returns:
            torch.Tensor: Возвращает тензор лоссов после прохода модели.
        """
        x, y = sample
        logits = self.forward(x.to(torch.float32))
        preds = torch.argmax(logits, 1)
        loss = self.criterion(logits, y.to(torch.int64))
        # собираем в атрибут
        self.preds_stage[stage]['loss'].append(loss.detach().cpu())
        self.preds_stage[stage]['preds'].append(preds.detach().cpu())
        self.preds_stage[stage]['targets'].append(y.cpu())
        return loss

    def shared_epoch_end(self,
                         stage: str = None) -> None:
        """Функция расчета лоссов и точности в конце эпохи
        и логгирования этих значений

        Args:
            stage (str, required): Этап, может принимать значения из списка:
            ['train', 'valid', 'test']. По умолчанию None.
        """
        # считаем по стадиям
        loss = self.preds_stage[stage]['loss']
        loss = torch.stack(loss)
        loss = np.mean([x.item() for x in loss])

        self.preds_stage[stage]['preds'] = (torch.cat(
            self.preds_stage[stage]['preds'])).tolist()

        self.preds_stage[stage]['targets'] = (torch.cat(
            self.preds_stage[stage]['targets'])).tolist()

        acc = accuracy_score(self.preds_stage[stage]['targets'],
                             self.preds_stage[stage]['preds'])

        f1_macro = f1_score(self.preds_stage[stage]['targets'],
                      self.preds_stage[stage]['preds'],
                      average='macro',
                      zero_division=0)

        f1_weighted = f1_score(self.preds_stage[stage]['targets'],
                      self.preds_stage[stage]['preds'],
                      average='weighted',
                      zero_division=0)

        if self.model_device == 'mps':
            loss = loss.astype(np.float32)
            acc = acc.astype(np.float32)
            f1_macro = f1_macro.astype(np.float32)
            f1_weighted = f1_weighted.astype(np.float32)

        metrics = {
            f"{stage}_loss": loss,
            f"{stage}_acc": acc,
            f"{stage}_f1_macro": f1_macro,
            f"{stage}_f1_weighted": f1_weighted
        }
        self.log_dict(metrics, prog_bar=True)

        if stage == "test":
            self.log_classification_report()

        # чистим
        self.preds_stage[stage]['loss'].clear()
        self.preds_stage[stage]['preds'].clear()
        self.preds_stage[stage]['targets'].clear()

    # создаем оптимизатор, шедулер
    def configure_optimizers(self) -> dict:
        """Функция создания оптимизатора и шедулера

        Returns:
            dict: Возвращает словарь вида
            {"optimizer": optimizer, "lr_scheduler": scheduler_dict},
            который содержит в себе оптимизатор и шедулер
        """
        optimizer = torch.optim.AdamW(
            self.parameters(),
            lr=self.hparams.eta
        )

        scheduler_dict = {
            "scheduler": torch.optim.lr_scheduler.ReduceLROnPlateau(
                optimizer,
                patience=5
            ),
            "interval": "epoch",
            "monitor": "valid_loss"
        }

        return {"optimizer": optimizer, "lr_scheduler": scheduler_dict}

    # шаг обучения
    def training_step(self,
                      batch=None,
                      batch_idx=None) -> torch.Tensor:
        """Функция обучения модели

        Args:
            batch_idx (_type_): Идентификатор батча
            batch (_type_, optional): Тензор, массив с изображениями.
            По умолчанию None.

        Returns:
            torch.Tensor: Возвращает тензор лоссов после прохода модели.
        """
        return self.shared_step(batch, "train")

    # шаг подсчета метрик обучения
    def on_training_epoch_end(self) -> None:
        """Функция расчета лоссов и точности на конце эпохи"""
        return self.shared_epoch_end("train")

    # шаг валидации
    def validation_step(self,
                        batch=None,
                        batch_idx=None) -> torch.Tensor:
        """Функция валидации модели

        Args:
            batch_idx (_type_): Идентификатор батча
            batch (_type_, optional): Тензор, массив с изображениями.
            По умолчанию None.

        Returns:
            torch.Tensor: Возвращает тензор лоссов после прохода модели.
        """
        return self.shared_step(batch, "valid")

    # шаг подсчета метрик валидации
    def on_validation_epoch_end(self) -> None:
        """Функция расчета лоссов и точности на конце эпохи"""
        return self.shared_epoch_end("valid")

    # шаг тестирования
    def test_step(self,
                  batch=None,
                  batch_idx=None) -> torch.Tensor:
        """Функция тестирования модели

        Args:
            batch_idx (_type_): Идентификатор батча
            batch (_type_, optional): Тензор, массив с изображениями.
            По умолчанию None.

        Returns:
            torch.Tensor: Возвращает тензор лоссов после прохода модели.
        """

        return self.shared_step(batch, "test")

    def log_classification_report(self):
        report_str = classification_report(self.preds_stage['test']['targets'],
                                    self.preds_stage['test']['preds'],
                                    target_names=self.class_names,
                                    zero_division=0)
        if self.cls_report_path is None:
            report_file = "classification_report.txt"
        else:
            report_file = self.cls_report_path
        with open(report_file, "w") as file:
            file.write(report_str)
        self.logger.experiment.log_artifact(run_id=self.logger.run_id,
                                            local_path=report_file)
        if self.cls_report_path is None:
            os.remove(report_file)

    # шаг подсчета метрик тестирования
    def on_test_epoch_end(self) -> None:
        """Функция расчета лоссов и точности на конце эпохи"""
        return self.shared_epoch_end("test")
