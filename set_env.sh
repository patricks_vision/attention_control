#!/bin/bash

# Укажите здесь путь к вашему .env файлу
env_file=".env"

# Проверяем, существует ли файл .env
if [ -f "$env_file" ]; then
    # Считываем файл построчно
    while IFS='=' read -r key value; do
        # Игнорируем строки, начинающиеся с '#' (комментарии) и пустые строки
        if [[ "$key" != \#* ]] && [[ -n "$key" ]]; then
            # Устанавливаем переменные среды
            export "$key"="$value"
        fi
    done < "$env_file"
    echo "Переменные среды успешно установлены из файла $env_file."
else
    echo "Файл $env_file не найден."
fi